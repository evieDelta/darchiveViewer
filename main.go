package main

import (
	"bytes"
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path"
	"strings"
)

func doViewer(r http.ResponseWriter, q *http.Request) error {
	uq := q.URL.Query()

	dest, err := url.QueryUnescape(uq.Get("page"))
	if err != nil {
		return err
	}

	if uq.Get("reload") == "true" {
		if err := tArchive.Reload(); err != nil {
			return err
		}
	}

	return loadAndGenPage(dest, r)
}

func pubView(r http.ResponseWriter, q *http.Request) {
	err := doViewer(r, q)
	if err != nil {
		r.WriteHeader(500)
		fmt.Println(err)
	}
}

const reloadMenu = `<html><body><form><input type="submit" name="reload"></form><br><p>%v</p></body></html>`

func pubReload(r http.ResponseWriter, q *http.Request) {
	fmt.Println("reloading templates")
	err := tArchive.Reload()
	if err != nil {
		r.WriteHeader(500)
		fmt.Println(err)

		fmt.Fprintf(r, reloadMenu, "Something has gone wrong")

		return
	}

	fmt.Fprintf(r, reloadMenu, template.HTMLEscaper(fmt.Sprint(err)))
}

var (
	flagFile string
	flagGen  string
	flagWeb  bool
	flagHost string
)

func main() {
	flag.StringVar(&flagFile, "file", "", "generate a local file from a local file directly")
	flag.StringVar(&flagGen, "direct", "", "generate a local file from a url directly instead of starting up the http server")
	flag.StringVar(&flagHost, "host", "127.0.0.1:13120", "the host address and port to respond to")
	flag.BoolVar(&flagWeb, "web", false, "host a webserver of it instead")
	flag.Parse()

	if flagWeb {
		http.HandleFunc("/view", pubView)
		http.HandleFunc("/reload", pubReload)
		http.Handle("/static/", http.FileServer(http.Dir("pub")))

		fmt.Println("Starting webserver on address ", flagHost)
		if err := http.ListenAndServe(flagHost, http.DefaultServeMux); err != nil {
			fmt.Println(err)
		}
		fmt.Println("Shutting down server")
	} else if flagGen != "" {
		css, err := ioutil.ReadFile("./pub/static/discord.css")
		if err != nil {
			panic(err)
		}

		dat := bytes.NewBufferString("")
		err = loadAndGenPage(flagGen, dat)
		if err != nil {
			panic(err)
		}
		fmt.Fprint(dat, "<style>", string(css), "</style>")

		u, err := url.Parse(flagGen)
		if err != nil {
			panic(err)
		}

		_, f := path.Split(u.Path)
		name := f[:strings.Index(f, ".")]

		err = ioutil.WriteFile(name+".html", dat.Bytes(), 0660)
		if err != nil {
			panic(err)
		}
	} else if flagFile != "" {
		css, err := ioutil.ReadFile("./pub/static/discord.css")
		if err != nil {
			panic(err)
		}

		dat := bytes.NewBufferString("")
		err = genFromFile(flagFile, dat)
		if err != nil {
			panic(err)
		}
		fmt.Fprint(dat, "<style>", string(css), "</style>")

		name := path.Base(flagFile)[:strings.Index(path.Base(flagFile), ".")]

		err = ioutil.WriteFile(name+".html", dat.Bytes(), 0660)
		if err != nil {
			panic(err)
		}
	} else {
		flag.PrintDefaults()
		os.Exit(1)
	}
}
