package main

import (
	"compress/gzip"
	"encoding/json"
	"html/template"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"strings"
	"time"

	"git.lan/darchiveviewer/datafmt"
)

var client = http.Client{
	Timeout: time.Minute,
}

const templateDir = "./templates/"
const templateExt = ".html"
const (
	templateArchive = "archive"
	templateViewer  = "archiveViewer"
	templateHeader  = "archiveHeader"
	templateBody    = "archiveData"
	templateFooter  = "archiveFooter"
)

type templatearchivedata struct {
	root *template.Template
}

var tArchive = templatearchivedata{}

func (t *templatearchivedata) Reload() (err error) {
	oldt := t

	/*t.root, err = template.New("root").Funcs(temfuncs).Parse("Hewwo")
	if err != nil {
		t = oldt
		return err
	}
	t.viewer, err = t.root.New(templateArchive).ParseFiles(path.Join(templateDir, templateArchive+templateExt))
	if err != nil {
		t = oldt
		return err
	}
	t.header, err = t.root.New(templateHeader).ParseFiles(path.Join(templateDir, templateHeader+templateExt))
	if err != nil {
		t = oldt
		return err
	}*/
	t.root, err = template.New("root").Funcs(temfuncs).ParseGlob(path.Join(templateDir, templateArchive+"*"+templateExt))
	if err != nil {
		t = oldt
		return err
	}

	return nil
}

func init() {
	if err := tArchive.Reload(); err != nil {
		panic(err)
	}
}

func readJSON(data []byte) (adat datafmt.ArchiveData, err error) {
	err = json.Unmarshal(data, &adat)
	if err != nil {
		return adat, err
	}

	if adat.PackVersion == 0 {
		err = json.Unmarshal(data, &adat.Messages)
		if err != nil {
			return adat, err
		}
	}

	return
}

func uncompressGZ(data io.Reader) ([]byte, error) {
	gz, err := gzip.NewReader(data)
	if err != nil {
		return nil, err
	}
	return ioutil.ReadAll(gz)
}

func loadAndGenPage(aurl string, writer io.Writer) error {
	r, err := client.Get(aurl)
	if err != nil {
		return err
	}

	/*uri, err := url.ParseRequestURI(aurl)
	if err != nil {
		return err
	}
	var ext string
	switch {
	case strings.HasSuffix(uri.RawQuery, "json"):
		ext = "json"
	case strings.HasSuffix(uri.RawQuery, "json.gz"):
		ext = "json.gz"
	}*/

	var dat []byte
	var jdat datafmt.ArchiveData

	switch r.Header.Get("Content-Type") {
	case "application/gzip":
		dat, err = uncompressGZ(r.Body)
		if err != nil {
			return err
		}
	case "application/json":
		dat, err = ioutil.ReadAll(r.Body)
		if err != nil {
			return err
		}
	}

	jdat, err = readJSON(dat)
	if err != nil {
		return err
	}

	return genPage(jdat, writer)
}

func genFromFile(filename string, writer io.Writer) error {
	fl, err := os.Open(filename)
	if err != nil {
		return err
	}

	var ext string
	switch {
	case strings.HasSuffix(filename, "json"):
		ext = "json"
	case strings.HasSuffix(filename, "json.gz"):
		ext = "json.gz"
	}

	var dat []byte
	var jdat datafmt.ArchiveData

	switch ext {
	case "json.gz":
		dat, err = uncompressGZ(fl)
		if err != nil {
			return err
		}
	case "json":
		dat, err = ioutil.ReadAll(fl)
		if err != nil {
			return err
		}
	}

	jdat, err = readJSON(dat)
	if err != nil {
		return err
	}

	return genPage(jdat, writer)
}

func genPage(dat datafmt.ArchiveData, writer io.Writer) error {
	return tArchive.root.ExecuteTemplate(writer, templateViewer, dat)
}
